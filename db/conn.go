package db

import (
	"errors"
	"fmt"
	"os"

	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"

	_ "github.com/mattn/go-sqlite3"
)


func InitSqliteDriver() *SqliteDriver {
	user := os.Getenv("USER")
	name := os.Getenv("NAME")
	password := os.Getenv("PASSWORD")
	host := os.Getenv("HOST")
	dbPort := os.Getenv("DBPORT")
	return &SqliteDriver{
		C: &Cred{
			User:     user,
			Name:     name,
			Password: password,
			Host:     host,
			Port:     dbPort,
		},
	}
}

//GetConnection return a database connection
func (s *SqliteDriver) GetConnection( local bool) (*sqlx.DB, error) {
	cred := s.C

	if local {
		cred = &Cred{
			User:     "admin",
			Password: "admin",
			Name:     "db",
			Port:     "5432",
			Host:     "localhost",
		}
	}

	db, err := sqlx.Connect("sqlite3", fmt.Sprintf("user=%s password=%s dbname=%s port=%s host=%s sslmode=disable", cred.User, cred.Password, cred.Name, cred.Port, cred.Host))
	if err != nil {
		log.Println(err)
		return nil, errors.New("connection failed")
	}


	if err := createTables(db); err != nil {
		return nil, err
	}

	log.Println("Stablish connection")

	return db, nil
}