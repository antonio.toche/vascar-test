package db

import (
	"reflect"
	"strings"

	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
)

// createTables, create schemas if not exist
func createTables(db *sqlx.DB) error {

	_, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS posts
	 	(postID text NOT NULL primary key, 
	  	postText VARCHAR(500) NOT NULL,
	   	creationDate DATE NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	   	userCreator integer NOT NULL)
	`)
	if err != nil {
		log.Fatal(err)
		return err
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS comments
	 	(commentID text NOT NULL primary key, 
	  	commentText VARCHAR(500) NOT NULL,
	   	creationDate DATE NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	   	userCreator integer NOT NULL)
		`)
	if err != nil {
		log.Fatal(err)
		return err
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS reports
	 	(reportID text NOT NULL primary key,
		reportType text NOT NULL, 
	  	reason  NOT NULL,
	   	creationDate DATE NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	   	userCreator integer NOT NULL)
		`)
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}

// CompleteQuery complete query select * with fields of struct, this
// is helpfully when we change the schema and don't want to crash anything
func CompleteQuery(query string, strct interface{}) string {
	queryTags := strings.Fields(strings.TrimSpace(query))
	if strings.ToLower(queryTags[0]) == "select" {
		if queryTags[1] == "*" {
			tags := getTags(strct)
			query = strings.Replace(query, "*", tags, 1)
		}
	}

	return query
}

// getTags return the value of db tags givin in a struct
func getTags(s interface{}) string {
	rt := reflect.TypeOf(s)
	if rt.Kind() != reflect.Struct {
		panic("bad type")
	}
	var result string
	for i := 0; i < rt.NumField(); i++ {
		f := rt.Field(i)
		if len(f.Tag.Get("db")) > 0 {
			if len(result) > 0 {
				result += ","
			}
			result += strings.Split(f.Tag.Get("db"), ",")[0]
		}
	}
	return result
}
