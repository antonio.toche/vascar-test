package db

import "github.com/jmoiron/sqlx"

type Cred struct {
	User     string
	Password string
	Name     string
	Port     string
	Host     string
}


type Store interface {
	GetConnection(local bool) (*sqlx.DB, error)
}

type SqliteDriver struct {
	C *Cred
}