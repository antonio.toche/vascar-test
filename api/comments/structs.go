package comments


import "time"

type Comments struct {
	CommentID    string    `json:"commentID" db:"commentID"`
	Text         string    `json:"text" db:"commentText"`
	CreationDate time.Time `json:"creationDate" db:"creationDate"`
	UserCreator  int       `json:"-" db:"userCreator"`
}

