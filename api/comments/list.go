package comments

import (
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	dbtest "socialtest/db"
)

func ListComments(db *sqlx.DB, limit, offset string) ([]*Comments, int, error)  {
	query := dbtest.CompleteQuery("select * from comments limit $1 offset $2", Comments{})

	comments := []*Comments{}
	if err := db.Select(&comments, query, limit, offset); err != nil {
		log.Error(err)
		return nil, fiber.StatusInternalServerError, err
	}

	if len(comments) == 0 {
		return []*Comments{}, fiber.StatusAccepted, nil
	}

	return comments, fiber.StatusOK, nil
}