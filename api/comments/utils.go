package comments

import "errors"

func (s *Comments) Validate() error {
	if len(s.Text) > 500 {
		return errors.New("limit of characters available exceed")
	}

	return nil
}
