package post

import "errors"

func (s *Posts) Validate() error {
	if len(s.Post) > 500 {
		return errors.New("limit of characters available exceed")
	}

	return nil
}
