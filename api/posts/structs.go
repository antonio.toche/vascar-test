package post

import "time"

type Posts struct {
	PostID       string    `json:"postID" db:"postID"`
	Post         string    `json:"post" db:"postText"`
	CreationDate time.Time `json:"creationDate" db:"creationDate"`
	UserCreator  int       `json:"-" db:"userCreator"`
}
