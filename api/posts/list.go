package post

import (
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	dbtest "socialtest/db"
)

func ListPosts(db *sqlx.DB, limit, offset string) ([]*Posts, int, error) {
	query := dbtest.CompleteQuery("select * from posts limit $1 offset $2", Posts{})

	posts := []*Posts{}
	if err := db.Select(&posts, query, limit, offset); err != nil {
		log.Error(err)
		return nil, fiber.StatusInternalServerError, err
	}

	if len(posts) == 0 {
		return []*Posts{}, fiber.StatusAccepted, nil
	}

	return posts, fiber.StatusOK, nil
}
