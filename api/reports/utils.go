package report

import (
	"errors"

	vld "github.com/microcosm-cc/bluemonday"
)


// Validate the input
func (r *Report) Validate() error {
	if r.UserCreator == 0 {
		return errors.New("user is missing")
	}

	re, err := validateText(r.Reason)
	if err != nil {
		return err
	}

	r.Reason = re

	if err := r.ReportType.isValid(); err != nil  {
		return err
	}

	return nil
}


// isValid check if the type given is valid
func (r *ReportType) isValid() error {
	switch *r {
	case POST, COMMENT:
		return nil
	}
	return errors.New("invalid report type")
}


// validateText check if the text is valid or contains a script
func validateText(text string) (string, error) {
	p := vld.UGCPolicy()
	commentText := p.Sanitize(text)

	if len(commentText) == 0 {
		return "", errors.New("the reason is not valid")
	}
	return commentText, nil
}
