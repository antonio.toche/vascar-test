package report

import "time"


type ReportType string

const (
	POST ReportType = "post"
	COMMENT ReportType = "comment"
)

type Report struct {
	ReportID     string     `json:"reportID" db:"reportID"`
	Reason       string     `json:"reason" db:"reason"`
	ReportType   ReportType `json:"reportType" db:"reportType"`
	CreationDate time.Time  `json:"creationDate" db:"creationDate"`
	UserCreator  int        `json:"userCreator" db:"userCreator"`
}


type ReportRequest struct {
	Reason string `json:"reason"`
	UserCreator int `json:"userCreator"`
	ReportType ReportType `json:"-"`
}