package report

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

func InsertReport(db *sqlx.DB, r *ReportRequest) (int,error) {
	query := "insert into reports(reportID,reason,reportType,creationDate,userCreator) values (:reportID,:reason,:reportType,:creationDate,:userCreator)"

	newR := &Report{
		ReportID: uuid.New().String(),
		Reason: r.Reason,
		ReportType: r.ReportType,
		CreationDate: time.Now(),
		UserCreator: r.UserCreator,
	}

	if err := newR.Validate(); err != nil {
		return fiber.StatusBadRequest, err
	}


	_, err := db.NamedExec(query, &newR)
	if err != nil {
		return fiber.StatusInternalServerError, err
	}


	return fiber.StatusCreated, nil
}