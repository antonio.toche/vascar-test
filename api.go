package socialtest

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	log "github.com/sirupsen/logrus"
	db "socialtest/db"
)

/*
	Creates a new API with util functions
	local: if the running with be in local test, dev or production
	port: port of the api
*/
func NewApi(s db.Store, port string, local bool) (*api, error) {
	// Connection of db
	conn, err := s.GetConnection(local)
	if err != nil {
		return nil, err
	}

	// Create fiber router and app
	fiberRouter := fiber.New()

	// Set cors policy with default config
	fiberRouter.Use(cors.New(cors.Config{
		AllowOrigins: "*",
	}))

	return &api{
		App:               fiberRouter,
		Port:              port,
		DBWriterAndWriter: conn,
	}, nil
}

// Entrypoint, launch the server
func (a *api) Launch() error {
	// Routes without auth
	a.RegisterUnprotectedRoutes()
	return a.App.Listen(a.Port)
}

type handler func(*fiber.Ctx) (string, error)

// Handler is a observer in which we can log anything
func (a *api) Handler(c *fiber.Ctx, fn handler) error {
	log.Println("Reading ", c.Route().Method, c.Route().Path)
	history := &HistoricAdminRequest{
		Method:   c.Route().Method,
		Resource: c.Route().Path,
	}

	res, err := fn(c)
	if err != nil {
		history.Message = res
		log.Error(err)
		return err
	}

	log.Println(res)
	return nil
}
