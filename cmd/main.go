package main

import (
	"os"
	t "socialtest"
	"socialtest/db"
	"strconv"

	env "github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
)

func main() {
	err := env.Load("../.env")
	if err != nil {
		log.Error(err)
		return
	}

	port := ":" + os.Getenv("PORT")
	local, _ := strconv.ParseBool(os.Getenv("LOCAL"))

	api, err := t.NewApi(db.InitSqliteDriver(), port, local)
	if err != nil {
		panic(err)
	}

	defer api.DBWriterAndWriter.Close()


	if err := api.Launch(); err != nil {
		panic(err)
	}
}
