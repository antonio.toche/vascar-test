module socialtest

go 1.15

require (
	github.com/gofiber/fiber/v2 v2.35.0
	github.com/google/uuid v1.3.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/joho/godotenv v1.4.0
	github.com/mattn/go-sqlite3 v1.14.14
	github.com/microcosm-cc/bluemonday v1.0.19
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.7.0
	gopkg.in/check.v1 v0.0.0-20161208181325-20d25e280405
)
