package socialtest

import (
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
)


type api struct {
	DBWriterAndWriter *sqlx.DB
	App               *fiber.App
	Port              string
}


type HistoricAdminRequest struct {
	CreatedBy string `json:"createdBy"`
	Resource  string `json:"resource"`
	Method    string `json:"method,omitempty"`
	Message   string `json:"message,omitempty"`
}
