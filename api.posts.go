package socialtest

import (
	"github.com/gofiber/fiber/v2"
	post "socialtest/api/posts"
)

func (a *api) Posts() error {
	c := a.App.Group("/post")

	c.Get("/", func(c *fiber.Ctx) error {
		return a.Handler(c, func(c *fiber.Ctx) (string, error) {
			limit := c.Query("limit", "100")
			offset := c.Query("offset", "0")

			listComments, code, err := post.ListPosts(a.DBWriterAndWriter, limit, offset)
			if err != nil {
				return "cant get list of posts", c.Status(code).JSON(fiber.Map{
					"error": "we are having issues with our services, please try again later.",
				})
			}

			return "List posts finish without issues", c.Status(fiber.StatusAccepted).JSON(fiber.Map{
				"posts": listComments,
			})
		})
	})

	return nil
}
