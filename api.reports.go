package socialtest

import (
	"fmt"
	r "socialtest/api/reports"

	"github.com/gofiber/fiber/v2"
)

func (a *api) Reports() error {
	c := a.App.Group("/reports")

	c.Post("/comments", func(c *fiber.Ctx) error {
		return a.Handler(c, func(c *fiber.Ctx) (string, error) {
			repReq := r.ReportRequest{
				ReportType: "comment",
			}
			if err := c.BodyParser(&repReq); err != nil {
				return "error while parsing body", c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
					"error": "error in the input",
				})
			}

			code, err := r.InsertReport(a.DBWriterAndWriter, &repReq)
			if err != nil {
				return "error while inserting new comment", c.Status(code).JSON(fiber.Map{
					"error": "error while trying to insert new item on reports",
				})
			}
			return fmt.Sprintf("[%d] new comment created successfully", repReq.UserCreator), c.Status(code).JSON(fiber.Map{
				"data": "new comment created successfully",
			})
		})
	})

	c.Post("/posts", func(c *fiber.Ctx) error {
		return a.Handler(c, func(c *fiber.Ctx) (string, error) {
			repReq := r.ReportRequest{}
			if err := c.BodyParser(&repReq); err != nil {
				return "error while parsing body", c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
					"error": "error in the input",
				})
			}

			repReq.ReportType = "post"
			code, err := r.InsertReport(a.DBWriterAndWriter, &repReq)
			if err != nil {
				return "error while inserting new element", c.Status(code).JSON(fiber.Map{
					"error": "error while trying to insert new item on reports",
				})
			}
			return fmt.Sprintf("[%d] new post created successfully", repReq.UserCreator), c.Status(code).JSON(fiber.Map{
				"data": "new post created successfully",
			})
		})
	})

	return nil
}
