package test

import (
	"encoding/json"

	"log"
	"net/http"
	"net/http/httptest"
	"os"
	sctest "socialtest"
	"socialtest/db"
	"strconv"
	"strings"
	"testing"

	"github.com/gofiber/fiber/v2"
	env "github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
)

func mockServer() *fiber.App {

	err := env.Load("../.env")
	if err != nil {
		panic(err)
	}

	port := ":" + os.Getenv("PORT")
	local, _ := strconv.ParseBool(os.Getenv("LOCAL"))

	api, err := sctest.NewApi(db.InitSqliteDriver(), port, local)
	if err != nil {
		panic(err)
	}

	api.Launch()
	return api.App

}

func TestReports(t *testing.T) {

	app := mockServer()

	tests := []struct {
		description   string
		expectedCode  int
		route         string
		reportRequest struct {
			Reason      string `json:"reason"`
			UserCreator int `json:"userCreator"`
		}
	}{
		{
			description:  "POST HTTP status 201",
			route:        "/reports/posts",
			expectedCode: 201,
			reportRequest: struct {
				Reason      string `json:"reason"`
				UserCreator int `json:"userCreator"`
			}{
				Reason:      "some",
				UserCreator: 12313,
			},
		},
		{

			description:  "POST HTTP status 201",
			route:        "/reports/comments",
			expectedCode: 201,
			reportRequest: struct {
				Reason      string `json:"reason"`
				UserCreator int `json:"userCreator"`
			}{
				Reason:      "someanotehr",
				UserCreator: 123132,
			}},
	}

	for _, test := range tests {
		content, _ := json.Marshal(test.reportRequest)

		req := httptest.NewRequest(http.MethodPost, test.route,strings.NewReader(string(content)))
		req.Header.Set("Content-Length", strconv.Itoa(len(string(content))))
		req.Header.Set("Content-Type", "application/json")

		resp, err := app.Test(req)
		if err != nil {
			log.Println(err)
			t.FailNow()
		}

		if	!assert.Equalf(t, test.expectedCode, resp.StatusCode, test.description) {
			t.Fail()
		}
	  }
}
